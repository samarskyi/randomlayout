package training.global.sgv.randomlayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RemoteViews;

import java.util.Random;

/**
 * Created by eugenii.samarskyi on 10/14/2014.
 */
@RemoteViews.RemoteView
public class RandomCustomLayout extends ViewGroup {

    private static final String TAG = RandomCustomLayout.class.getSimpleName();
    private Context context;
    private static final int SIZE = 50;

    public RandomCustomLayout(Context context) {
        super(context);
    }

    public RandomCustomLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RandomCustomLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new RandomCustomLayout.LayoutParams(getContext(), attrs);
    }

    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof RandomCustomLayout.LayoutParams;
    }

    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new RandomCustomLayout.LayoutParams(p);
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 0, 0);
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public static class LayoutParams extends ViewGroup.LayoutParams {

        public int x;
        public int y;

        /**
         * Creates a new set of layout parameters with the specified width,
         * height and location.
         *
         * @param width  The width, either {@link #MATCH_PARENT}, {@link #WRAP_CONTENT} or a fixed size in pixels
         * @param height The height, either {@link #MATCH_PARENT}, {@link #WRAP_CONTENT} or a fixed size in pixels
         * @param x      The X location of the child
         * @param y      The Y location of the child
         */
        public LayoutParams(int width, int height, int x, int y) {
            super(width, height);
            this.x = x;
            this.y = y;
        }

        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
            TypedArray a = c.obtainStyledAttributes(attrs, R.styleable.CustomRandomLayout);
            x = a.getDimensionPixelOffset(R.styleable.CustomRandomLayout_layout_x, 0);
            y = a.getDimensionPixelOffset(R.styleable.CustomRandomLayout_layout_y, 0);
            a.recycle();
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }


        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();

        int maxHeight = getHeight();
        int maxWidth = getWidth();


        setMeasuredDimension(resolveSize(maxWidth, widthMeasureSpec), resolveSize(maxHeight, heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);

            // add border
            child.setBackgroundResource(R.drawable.my);
            if (child.getVisibility() != GONE) {

                LayoutParams layoutParams = (LayoutParams) child.getLayoutParams();
                if (layoutParams == null || layoutParams.width == 0 || layoutParams.height == 0
                        || (layoutParams.x == 0 && layoutParams.y == 0)) {
                    layoutParams = createRandomLayoutParams();
                    child.setLayoutParams(layoutParams);
                }

                int leftCur = layoutParams.x;
                int topCur = layoutParams.y;

                Log.d(TAG, " View position : left point = " + leftCur + ", top point = " + topCur);
                child.layout(leftCur, topCur, leftCur + layoutParams.width, topCur + layoutParams.width);
                invalidate();
            }
        }
    }

    private LayoutParams createRandomLayoutParams() {
        Random r = new Random();
        LayoutParams layoutParams = new LayoutParams(SIZE, SIZE, r.nextInt(getWidth()), r.nextInt(getHeight()));
        return layoutParams;
    }

    public void changePositions() {
        Random r = new Random();
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                // animation
                child.animate().x(r.nextInt(getWidth())).y(r.nextInt(getWidth()));
            }
        }
        invalidate();
    }

    public void addView() {
        ImageView mImageView = new ImageView(context);
        mImageView.setBackgroundResource(R.drawable.my);
        mImageView.setImageResource(android.R.drawable.btn_star);
        mImageView.setLayoutParams(createRandomLayoutParams());
        addView(mImageView);
    }

}
