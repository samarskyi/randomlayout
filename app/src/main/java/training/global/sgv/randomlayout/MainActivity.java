package training.global.sgv.randomlayout;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;


public class MainActivity extends Activity {

    private RandomCustomLayout mRandomCustomLayout;
    private Button mChangeButton;
    private SeekBar mSeekBar;
    private ImageView myButton;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRandomCustomLayout = (RandomCustomLayout) findViewById(R.id.random_layout);
        mChangeButton = (Button) findViewById(R.id.button_change);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar);

        myButton = new ImageView(getApplicationContext());
        myButton.setBackgroundResource(R.drawable.my);
        myButton.setImageResource(android.R.drawable.btn_star);


        mRandomCustomLayout.setContext(getApplicationContext());
        mRandomCustomLayout.addView(myButton);
        mRandomCustomLayout.invalidate();

        mChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRandomCustomLayout.changePositions();
                Log.d(TAG, mRandomCustomLayout.getChildCount() + " count of children");
            }
        });

        mSeekBar.setMax(20);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            //
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // add new view
                if (mRandomCustomLayout.getChildCount() < progress) {
                    mRandomCustomLayout.addView();
                }
                // remove view
                if (mRandomCustomLayout.getChildCount() > progress) {
                    mRandomCustomLayout.removeViewAt(0);
                    mRandomCustomLayout.invalidate();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
